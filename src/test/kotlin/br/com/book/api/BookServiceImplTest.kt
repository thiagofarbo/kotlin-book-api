package br.com.book.api

import br.com.book.api.domain.Book
import br.com.book.api.repository.BookRepository
import br.com.book.api.repository.RentalRepository
import br.com.book.api.repository.RenterRepository
import br.com.book.api.service.BookServiceImpl
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort


@ExtendWith(MockitoExtension::class)
class BookServiceImplTest {

    @Mock
    private lateinit var bookRepository: BookRepository

    @Mock
    private lateinit var renterRepository: RenterRepository

    @Mock
    private lateinit var rentalRepository: RentalRepository

    @InjectMocks
    private lateinit var bookService: BookServiceImpl

    @Test
    fun testSave() {
        val bookToSave = Book()

        `when`(bookRepository.save(bookToSave)).thenReturn(bookToSave)

        val savedBook = bookService.save(bookToSave)

        assertEquals(bookToSave, savedBook)
    }

    @Test
    fun testList() {
        val isbn = "123456789"
        val name = "Test Book"

        `when`(bookRepository.findAllByIsbnOrTitle(isbn, name, PageRequest.of(0, 10, Sort.by("title")))).thenReturn(Page.empty())

        val result: Page<Book> = bookService.list(isbn, name, PageRequest.of(0, 10, Sort.by("title")))

        assertTrue(result.isEmpty)

    }

}